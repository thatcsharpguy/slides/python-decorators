---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
---

# Python decorators

Decorating your Python code

---
layout: image-right
image: https://source.unsplash.com/collection/94734566/1920x1080
---

# Flask

You may have seen a code snippet like this[^1]

```python
app = Flask("app")

@app.route("/")
def index():
  return "Hello World!"
```

But, have you ever stopped to think about what does that `@app.route("/")` means?

[^1]: [Flask – A Minimal Application](https://flask.palletsprojects.com/en/2.0.x/quickstart/#a-minimal-application)

<style>
.footnotes-sep {
  @apply mt-20 opacity-10;
}
.footnotes {
  @apply text-sm opacity-75;
}
.footnote-backref {
  display: none;
}
</style>

---

# In Python

- 🎀 **Decorators** - their Python name
- They are a **design pattern**
- Useful to **attach behaviours** to functions
- No code modification

<hr />

Read the [PEP 318](https://www.python.org/dev/peps/pep-0318/)

<br>
<br>

- But before...

---

# Functions  – first class citizens

Functions are like any other object:

 1. Be passed as arguments to functions
 2. Be returned as result of functions

```python {all|1-2|4|10|6-9|12|14|all}
def greeteer(human):
  return f'Hi {human}'

def yell(fn):

  def wrapper(human):
    result = fn(human)
    return f'{result} !!!'

  return wrapper

yelling_greeter = yell(greeter)

yelling_greeter('Python')
```

---
layout: center
class: text-center
---

# Yes, but why?

Cleanliness · Code reuse · Enforcing


---

# Sweet, sweet syntax sugar – `@`

Calling functions, with functions as arguments like when we created our `yelling_greeter` may become tedious.

```python
def yell(fn):
  def wrapper(human):
    return f'{fn(human)} !!!'
  return wrapper
```

```python
@yell
def greeteer(human):
  return f'Hi {human}'

yelling_greeter('Python')
```

Roughly the same as

```python
greeteer = yell(greeteer)

yelling_greeter('Python')
```

---

# What about OOP?

We can use classes – and their methods as decorators too!

```python
class Emotion:
  def __init__(self, symbol):
    self.symbol = symbol

  def add(self, fn):
    def _wrapper(content):
      result = fn(content)
      return f"{result} {self.symbol}"
    return _wrapper
```

Then we can do:

```python
emo = Emotion('?')

@emo.add
def goodbyer(element):
  return f"Bye {element}"
```

---

# Function's identity crisis

In general, that is all we need to create decorators, but...

<div grid="~ cols-2 gap-2" m="-t-2">

```python
def is_cool(function):
  def envoltura():
    """Just a cool function wrapper"""
    return function() + " is cool!"
  return envoltura

@is_cool
def python():
  """Returns the name of the best 
  programming language
  """
  return "Python"
```

```python {3|1-4|5-9|all}
# What is the name of the function?
print(python.__name__)

# > envoltura

# Can we see the docs?
print(python.__doc__)

# > Just a cool function wrapper
```

</div>

As always, there is a solution.

---

# A decorator's decorator 

Yep, a decorator for our decorator is the solution – **`from functools import wraps`**

<div grid="~ cols-2 gap-2" m="-t-2">

```python {all|1-3|all}
def is_cool(function):
  @wraps(function)
  def envoltura():
    """Just a cool function wrapper"""
    return function() + " is cool!"
  return envoltura

@is_cool
def python():
  """Returns the name of the best 
  programming language
  """
  return "Python"
```

```python {3|1-4|5-9|all}
# What is the name of the function?
print(python.__name__)

# > python

# Can we see the docs?
print(python.__doc__)

# > Returns the name of the best programming language
```

</div>

You can think of `@wraps` as a function that copies the metadata from one function to the other.

---

# What about the arguments, huh?

Flask uses `@app.route("/")`, not just `@app.route`, how can we fix that? – DOUBLE NESTED FUNCTIONS!!

```python {all|6-10|all}
class FlaskClone:
  def __init__(self, app_name):
    self.app_name = app_name
    self.route_map = dict()

  def route(self, route_name): # Receive the route name

    def _route_inner(function): # Receive the function to decorate

      def _actual_route_processor(web_request): # Actual function executor
        # Dummy code
        print(f"Executing the function related to "
              f"the route {route_name} within the app {self.app_name}")
        return function(web_request)
      
      self.route_map[route_name] = _actual_route_processor
      return _actual_route_processor

    return _route_inner  
```

---

# Arguments, yay!

```python 
app = FlaskClone("Cool app")

@app.route("/")
def home(web_request):
    print("Hello world!")

@app.route("/about")
def about(web_request):
    print("This would be the about page?")
```

```python 
app.route_map["/"](0)
# > Executing the function related to the route /
# > Hello world!

app.route_map["/about"](0) 
# > Executing the function related to the route /about
# > This would be the about page?
```

---

# Decorators...

- They are a **design pattern**
- Useful to **attach behaviours** to functions
- No code modification.



<hr />

Read the [PEP 318](https://www.python.org/dev/peps/pep-0318/)

<br>
<br>

- But before...

---

# Learn More

[Documentations](https://sli.dev) · [GitHub](https://github.com/slidevjs/slidev) · [Showcases](https://sli.dev/showcases.html)
